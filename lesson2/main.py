# Необходимо создать репозиторий на одном из хабов. На Ваш выбор
# Реализовать функцию parse (см. модуль main.py). Создать Пул реквест (Мердж реквест) в ветку мастер с решением.
# Реализовать функцию parse_cookie (см. модуль main.py). Создать Пул реквест (Мердж реквест) в ветку мастер с решением.
#
#
# На каждую из реализаций сделать отдельную ветку

def parse(query: str) -> dict:
    return {}




if __name__ == '__main__':
    assert parse('https://example.com/path/to/page?name=ferret&color=purple') == {'name': 'ferret', 'color': 'purple'}
    assert parse('https://example.com/path/to/page?name=ferret&color=purple&') == {'name': 'ferret', 'color': 'purple'}
    assert parse('http://example.com/') == {}
    assert parse('http://example.com/?') == {}
    assert parse('http://example.com/?name=Dima') == {'name': 'Dima'}








def parse_cookie(query: str) -> dict:
    return {}


if __name__ == '__main__':
    assert parse_cookie('name=Dima;') == {'name': 'Dima'}
    assert parse_cookie('') == {}
    assert parse_cookie('name=Dima;age=28;') == {'name': 'Dima', 'age': '28'}
    assert parse_cookie('name=Dima=User;age=28;') == {'name': 'Dima=User', 'age': '28'}