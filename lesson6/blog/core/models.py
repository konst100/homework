from django.db import models
from django.contrib.auth.models import User

# Create your models here.



class Post(models.Model):
    title = models.CharField(max_length=255)
    body = models.TextField()
    author = models.ForeignKey(User, on_delete=models.CASCADE, related_name='posts')
    created_at = models.DateTimeField(auto_now_add=True)
    tags = models.ManyToManyField("core.Tags")

class Comment(models.Model):
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    post = models.ForeignKey(Post, on_delete=models.CASCADE)
    text = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)

class Tags(models.Model):
    name = models.CharField(max_length=255)


class Group(models.Model):
    title = models.TextField()

class Student(models.Model):
    title = models.TextField()





