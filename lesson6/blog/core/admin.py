from django.contrib import admin
# from django.contrib.auth.models import User
from core.models import Post, Comment, Tags, Group, Student
# Register your models here.

admin.site.register(Post)
admin.site.register(Comment)
admin.site.register(Tags)
admin.site.register(Group)
admin.site.register(Student)
