#from django.db import models

# Create your models here.
from django.db import models
from django.utils.timezone import now
from django.contrib.auth.models import User
from os.path import join


def get_images_path():
    curr_date = now().date()
    return join(*['products', 'all',
                  str(curr_date.year),
                  str(curr_date.month),
                  str(curr_date.day)])


class Product(models.Model):
    category = models.ForeignKey(
        "core.Category",
        on_delete=models.SET_NULL,
        null=True,
        blank=True
    )
    tags = models.ManyToManyField(
        'core.Tags',
        blank=True
    )
    description = models.TextField()
    price = models.DecimalField(max_digits=10, decimal_places=2)
    name = models.CharField(max_length=255)
    cover = models.ImageField(upload_to='products/covers')
    quantity = models.IntegerField(default=0)
    is_active = models.BooleanField(default=False)


    def __str__(self):
        return self.name


class Image(models.Model):
    image = models.ImageField(upload_to=get_images_path)
    product = models.ForeignKey(
        Product,
        on_delete=models.CASCADE,
        related_name='images'
    )  # Product => image_set

    # TODO On SIGNAL LESSON ADD REMOVE FILE

class Category(models.Model):
    name = models.CharField(max_length=255)
    description = models.TextField()
    cover = models.ImageField(upload_to='category')

    class Meta:
        unique_together = ('name', 'description')


    def __str__(self):
        return self.name


class Tags(models.Model):
    name = models.CharField(max_length=255, unique=True)


    def __str__(self):
        return self.name


class Cart(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, null=True)


class ProductInCart(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    cart = models.ForeignKey(Cart, on_delete=models.CASCADE)
    quantity = models.IntegerField()
    price = models.DecimalField(max_digits=10, decimal_places=2)

class Order(models.Model):

    STATUS_OPEN = 0
    STATUS_IN_PROGRESS = 1
    STATUS_READY_TO_SEND = 2
    STATUS_SEND = 3
    STATUS_COMPLETE = 4
    STATUS_CANCELED = 5
    STATUS_RETURNED = 6

    STATUS_CHOICES = (
        (STATUS_OPEN, 'Open'),
        (STATUS_IN_PROGRESS, "In Progress"),
        (STATUS_READY_TO_SEND, "Ready To Send"),
        (STATUS_SEND, "Send"),
        (STATUS_COMPLETE, "Complete"),
        (STATUS_CANCELED, "Canceled"),
        (STATUS_RETURNED, "Returned")
    )

    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    status = models.PositiveSmallIntegerField(choices=STATUS_CHOICES)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)



class ProductInOrder(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    quantity = models.IntegerField()
    price = models.DecimalField(max_digits=10, decimal_places=2)
    order = models.ForeignKey(Order, on_delete=models.CASCADE)


