def parse(query: str) -> dict:
    dictionary = {}

    length = len(query)
    i = 0

    try:
        i = query.index('?') + 1
    except:
        return dictionary

    while (i < length):
        try:
            index1 = query.index('=', i)
        except:
            return dictionary

        key = query[i:index1]

        try:
            index2 = query.index('&', i + 1)
        except:
            i = length
            index2 = length

        val = query[index1 + 1:index2]

        dictionary[key] = val
        i = index2 + 1
        print(dictionary)



    return dictionary


if __name__ == '__main__':
    assert parse('https://example.com/path/to/page?name=ferret&color=purple') == {'name': 'ferret', 'color': 'purple'}
    assert parse('https://example.com/path/to/page?name=ferret&color=purple&') == {'name': 'ferret', 'color': 'purple'}
    assert parse('http://example.com/') == {}
    assert parse('http://example.com/?') == {}
    assert parse('http://example.com/?name=Dima') == {'name': 'Dima'}