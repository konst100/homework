def parse_cookie(query: str) -> dict:
    dictionary = {}
    length = len(query)
    i = 0
    while (i < length):
        index1 = query.index('=', i)
        index2 = query.index(';', index1)

        key = query[i:index1]
        val = query[index1 + 1:index2]

        dictionary[key] = val

        i = index2 + 1
        print(dictionary)
    return dictionary


print('enter "stop" to stop program')

"""
n = input()
while(n != 'stop'):
    d = parse(n)
    print(d)
    n = input()
"""


if __name__ == '__main__':
    assert parse_cookie('name=Dima;') == {'name': 'Dima'}
    assert parse_cookie('') == {}
    assert parse_cookie('name=Dima;age=28;') == {'name': 'Dima', 'age': '28'}
    assert parse_cookie('name=Dima=User;age=28;') == {'name': 'Dima=User', 'age': '28'}