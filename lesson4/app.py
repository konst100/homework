from flask import Flask, render_template, request
app = Flask(__name__)

@app.route('/')
def hello_world():
    with open('db.txt') as db:
        data = db.readlines()


        return render_template(
            'index.html', **{
                'username': 'kiwi',
                'names': data,
                'query': request.values
            }

        )
@app.route('/about/')
def about_as():
    return "About us"





if __name__ == '__main__':
    app.run(
        host='127.0.0.1',
        port=5000,
        debug=True
    )